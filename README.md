# About the project

## The project to create a simple web app with Python and streamlit
* Web application helps Zyxel customers can montinor their device status on our cloud   
* Web application uses RESTful API to perform request to Nebula, and display the result 
 
  ![image](/uploads/4d82f11858ab845736f23060cf449387/image.png)

## Prerequisite
* You must have GKE cluster, and install GitLab agent to safety connect to the GitLab (Refer GitLab CICD with a Kubernetes cluster.pdf)

![image](/uploads/b524e7076b814d6a8b1c03efbaccf381/image.png)

## The CICD Pipeline 
* Pipeline flow: GitLab Repo ---[trigger]---> GitLab CI ---[build and push docker image]---> GitLab Container Registry ---[deploy] ---> GKE cluster

  ![image](/uploads/801250b812715e5db09d33e21b9e8d9d/image.png)

* Pipeline stages
  - Build and push docker image: Build docker image from Dockerfile,then push to GitLab Container Registry
  - Deploy application on gke: Deploy the web application on GKE cluster. Note that, for production environment, you should deploy the web application on staging environment beforehand. 
  - Check web application health: The web application be accessible by external load balancer service. This stage will get the loadBalancerIP, and use GET method to check the web application health.

  ![image](/uploads/bcbc6b7b3579d558d503dbd0f526c43a/image.png) 

## Monitoring and Notification

* To monitor GKE cluster using Prometeus & Grafana, you can deploy using Google Cloud Marketplace

* To monitor the events for the pipelines, go to Project > Settings > Integrations, then add Slack notification 

  ![image](/uploads/c2e6b7e97a4b8859d9300191f409d0ec/image.png)
  
* GitLab will send the notification to Slack when git commit, push,... or pipeline status changes

  ![image](/uploads/adc90fe97103e8a6db982872f338d04b/image.png)


